<?php

namespace app\modules\test;

class TestModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\test\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
