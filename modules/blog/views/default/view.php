<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\blog\models\Posts */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="posts-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>


	
	<div class="panel panel-default"> 
		<div class="panel-heading"><?= $model->title ?></div>
		<div class="panel-body">
			<?= Html::decode($model->text) ?>
		</div>
	</div>

	<div class="row"> 
  <div class="col-sm-4 col-sm-push-8">.col-sm-4 .col-sm-push-8</div> 
  <div class="col-sm-8 col-sm-pull-4">.col-sm-8 .col-sm-pull-4</div> 
</div>
</div>
