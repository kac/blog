<?php

namespace app\modules\blog\models;

use Yii;
use yii\web\UploadedFile;
use yii\helpers\BaseStringHelper;

/**
 * This is the model class for table "yii_page".
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property string $text_preview
 * @property string $img
 */
class Posts extends \yii\db\ActiveRecord
{

	public $image;
	public $filename;
	public $string;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'yii_page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'text'], 'required'],
            [['text'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['text_preview'], 'string', 'max' => 250],
			[['img'], 'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'text' => 'Text',
            'text_preview' => 'Text Preview',
            //'img' => 'Img',
        ];
    }
	public function beforeSave($insert) {
		if($this->isNewRecord){
			$this->string = substr(uniqid('img'), 0, 12);
			$this->image = UploadedFile::getInstance($this, 'img');
			$this->filename = 'static/images/' . $this->string . '.' . $this->image->extension;
			$this->image->saveAs($this->filename);
			
			$this->text_preview = BaseStringHelper::truncate($this->text, 250, '...');
			
			$this->img= '/' . $this->filename;
		}else{
			$this->img = UploadedFile::getInstance($this, 'images');
			if($this->img){
				$this->img->saveAs(substr($this->img, 1));
			}
		}
		return parent::beforeSave($insert);
}
}
